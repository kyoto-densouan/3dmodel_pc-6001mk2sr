# README #

1/3スケールのNEC PC-6001mk2SR風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1984年11月15日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-6000%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA#PC-6001mkIISR)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001mk2sr/raw/9a414aeb15cfd3dad0f241c05cd4b9e1f6b2c3b1/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001mk2sr/raw/9a414aeb15cfd3dad0f241c05cd4b9e1f6b2c3b1/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001mk2sr/raw/9a414aeb15cfd3dad0f241c05cd4b9e1f6b2c3b1/ExampleImage.png)
